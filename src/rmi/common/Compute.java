package rmi.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

import rmi.entity.Strategy;

public interface Compute extends Remote {
	
	public int add(int a, int b) throws RemoteException;
	
	public String deploy(Strategy strategy) throws RemoteException;
	
	//tat cac method deu phai throw RemoteException, neu ko chay se bao loi
}
