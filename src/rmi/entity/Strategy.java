package rmi.entity;

import java.io.Serializable;

public class Strategy implements Serializable {
	
	private static final long	serialVersionUID	= -6131362832039970224L;
	
	private String	name;
	
	public Strategy(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}
